#!/bin/bash

WAITTIME=5
AUTHLOG=`cat /var/log/auth.log`
LATESTLOGDATE=` date -I'seconds'`
CONNECTIONNUM=(`echo -e "$AUTHLOG" | grep 'sshd:session): session opened' | awk '{split($0,a," "); split(a[5],a,"[");split(a[2],a,"]"); print a[1]}'`)
LATESTAUTHLOG="$AUTHLOG"
LATESTSTRINGNUM="1"
WAITTIME=5
IFTTTKEY="B605EznhNyPkFetvl2DeZcTOgUg7G_2cTo2pbWBFgVcAXVTcdWsKwMMHoPx0Om-h"

# Find the first log entry after the script began to run
getstartposition () {
	STARTDATE=`date -I'seconds'`
	MAXLINES=`echo -e "$AUTHLOG" | wc -l`
	FIRSTLINENUM=1
	echo "Current date is $STARTDATE"
	while [[ $FIRSTLINENUM -lt $MAXLINES ]];do
		LOGDATE=`echo -e "$AUTHLOG" | tail -n +$FIRSTLINENUM | head -n 1 | awk '{split($0,a," "); print a[1], a[2], a[3]}' | head -n 1`
		LOGDATE=`echo "$LOGDATE" | date -I'seconds'`
		
		if [[ "$STARTDATE" < "$LOGDATE" ]];then
			FIRSTLINENUM=$MAXLINES
			LATESTAUTHLOG=`echo -e "$AUTHLOG" | tail -n +$FIRSTLINENUM`
			checklatestlogs $FIRSTLINENUM
			sleep $WAITTIME
			return
		else
			FIRSTLINENUM=$((FIRSTLINENUM + 1))
		fi
	done
}

checklatestlogs () {
	LATESTAUTHLOG=`echo -e "$AUTHLOG" | tail -n +$1`
	CONNECTIONNUM=(`echo -e "$LATESTAUTHLOG" | grep 'sshd:session): session opened' | awk '{split($0,a," "); split(a[5],a,"[");split(a[2],a,"]"); print a[1]}'`)
	for NUM in "${CONNECTIONNUM[@]}"
	do
		LASTSUCCCONN="$NUM"
		LASTSUCCCONNLINE=
		LASTSUCCCONNSTRING=`echo -e "$AUTHLOG" | grep "$NUM" | tail -n 1`
		HOSTNAME=`echo -e "$AUTHLOG" | grep "$NUM" | awk '{split($0,a," "); print a[4]}' | head -n 1`
		LOGSTRING=`echo -e "$AUTHLOG" | grep "$NUM" | awk '{split($0,a,":"); print a[4]}' | head -n 1`
		LOGDATE=`echo -e "$AUTHLOG" | grep "$NUM" | awk '{split($0,a," "); print a[1], a[2], a[3]}' | head -n 1`
		LOGDATE=`date -I'minutes' -d "$LOGDATE"`
		UUID=`uuidgen`
	
		echo -e "Connection num $NUM\n\thostname $HOSTNAME\n\tLOGDATE $LOGDATE\n\tLOGSTRING $LOGSTRING\n"
		
		curl -X POST -H "Accept: application/json" -H "Accept-Charset: utf-8" -H "Accept-Encoding: gzip, deflate" -H "User-Agent: IFTTT-Protocol/v1" -H "IFTTT-Channel-Key: $IFTTTKEY" -H "IFTTT-Service-Key: $IFTTTKEY" -H "X-IFTTT-Realtime: 1" -H "X-Request-ID: $UUID" -H "Content-Type: application/json" -d "{\"triggerFields\": {\"local_user_name\": \"usernametest\"},\"user\": {\"timezone\": \"America/Los_Angeles\"},\"ifttt_source\": {\"id\": \"119160588\",\"url\": \"https://ifttt.com/applets/ULS8BhUz\"},\"data\": {\"hostname\": \"$HOSTNAME\",\"logdate\": \"$LOGDATE\",\"logstring\": \"$LOGSTRING\"}}" https://api.maxlayer.net/ifttt/v1/triggers/login_trigger --output -
	done
	
	LATESTSTRINGNUM=`echo -e "$AUTHLOG" | wc -l`
}

### MAIN

getstartposition

while true
do
	LOGFILEDATE=`date -I'seconds' -r /var/log/auth.log`
	if [[ "$LATESTLOGDATE" > "$LOGFILEDATE" ]];then
		echo "Logfile NOT updated $LATESTLOGDATE > $LOGFILEDATE"
	else
		echo "logfile updated $LATESTLOGDATE < $LOGFILEDATE"
		AUTHLOG=`cat /var/log/auth.log`
		LATESTLOGDATE=`date -I'seconds'`
		LATESTAUTHLOG=`echo -e "$AUTHLOG" | tail -n +$LATESTSTRINGNUM`
		checklatestlogs $LATESTSTRINGNUM
	fi
	sleep $WAITTIME
done
